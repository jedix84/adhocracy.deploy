import sys
import os
import io
import re
import getopt
import subprocess
import ConfigParser
from time import sleep

import xml.dom.minidom as dom

class SystemCheck(object):
    def __init__(self):
        """Perform basic system check."""
        self.passed = True
        if not self.check_vboxmanage() or not self.check_vboxconfig():
            self.passed = False

    def check_vboxmanage(self):
        try:
            proc = subprocess.check_output("VBoxManage")
        except OSError as e:
            print "Could not execute 'VBoxManage'. It seems virtualbox is not installed on your system."
            print "Try: sudo apt-get install virtualbox"
            return False
        return True

    def check_vboxconfig(self):
        configfile = os.getenv("HOME") + "/.VirtualBox/VirtualBox.xml"
        try:
            self.machine_folder = dom.parse(configfile).getElementsByTagName("SystemProperties")[0].getAttribute("defaultMachineFolder")
        except IOError as e:
            print "VirtualBox config file not found. Does the following file exist?"
            return False
        return True
        
    def  check_configfile(self, configfile):
        return True

class Config(object):
    def __init__(self, configfile):
        self.config = ConfigParser.ConfigParser(allow_no_value=True)
        self.config.read(configfile)
        self.section = None

    def set_section(self, section):
        self.section = section

    def get_section(self, section=None):
        if section is None and self.section is None:
            return []
        elif section is None:
            section = self.section
        if self.config.has_section(section):
            return self.config.options(section)
        return []

    def get(self, option, section=None):
        if section is None and self.section is None:
            return None
        elif section is None:
            section = self.section
        if self.config.has_option(section, option):
            if not self.config.get(section, option) is None:
                return self.config.get(section, option)
            return True
        return None

class VMManager:
    def __init__(self, machine_folder):
        self.vms = self.get_vms()
        self.machine_folder = machine_folder

    def get_vms(self, get_running=False):
        vmtype = "vms"
        if get_running:
            vmtype = "running%s"%vmtype
        vmsoutput = subprocess.check_output(["VBoxManage", "list", vmtype])
        vmslist = vmsoutput.split("\n")[:-1]
        vms = []
        for vm in vmslist:
            res = re.search(r'"(.*)" {(.*)}', vm)
            vms.append([res.group(1), res.group(2)])
        return vms

    def show_vms(self):
        i = 0
        running_vms = self.get_vms(True)
        for vm in self.vms:
            i += 1
            if vm in running_vms:
                running = " (running)"
            else:
                running = ""
            print "%2d: %s (%s)%s" % (i, vm[0], vm[1], running)

class VM(object):
    def __init__(self, manager, configfile):
        self.manager = manager
        self.vmconfig = Config(configfile)
        self.vmconfig.set_section("VIRTUAL MACHINE SETTINGS")
        if not self.vmconfig.get("uid") is None:
            self.load_vm(self.vmconfig.get("uid"))
        elif not self.vmconfig.get("name") is None:
            self.create_vm()
        self.edit_vm()

    def load_vm(self, uid):
        try:
            vminfo = subprocess.check_output(["VBoxManage", "showvminfo", uid, "--machinereadable"], stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            print "Virtual machine with uid '%s' not found."%uid
            exit(1)
        root = "ROOT"
        vminfo = "[%s]\n%s" % (root, vminfo)
        config = ConfigParser.ConfigParser()
        config.readfp(io.BytesIO(vminfo))
        self.uid = config.get(root, "uuid")[1:-1]
        self.name = config.get(root, "name")[1:-1]
        self.memory = config.getint(root, "memory")
        self.cpus = config.getint(root, "cpus")
        self.ostype = config.get(root, "ostype")[1:-1]
        self.state = config.get(root, "VMState")[1:-1]

    def create_vm(self):
        self.name = self.vmconfig.get("name")
        self.ostype = self.vmconfig.get("ostype")
        vminfo = None
        try:
            vminfo = subprocess.check_output(["VBoxManage", "showvminfo", self.name, "--machinereadable"], stderr=open(os.devnull, 'w'))
        except:
            pass
        if not vminfo is None:
            if not self.vmconfig.get("delete-existing-vm"):
                print "A virtual machine named '%s' already exists." % self.name
                exit(1)
            else:
                root = "ROOT"
                vminfo = "[%s]\n%s" % (root, vminfo)
                config = ConfigParser.ConfigParser()
                config.readfp(io.BytesIO(vminfo))
                uid = config.get(root, "uuid")[1:-1]
                self.delete_vm(uid)

        command = ["VBoxManage", "createvm", "--name", self.name, "--register"]
        if self.ostype is not None:
            command.append("--ostype")
            command.append(self.ostype)
        try:
            subprocess.check_call(command, stdout=open(os.devnull, 'w'), stderr=open(os.devnull, 'w'))
        except subprocess.CalledProcessError as e:
            print "Could not create virtual machine."
            exit(1)

    def edit_vm(self):
        self.vmconfig.set_section("VIRTUAL MACHINE SETTINGS")
        self.memory = self.vmconfig.get("memory")
        self.cpus = self.vmconfig.get("cpus")
        self.modify_attribute(self.name, "--cpus", self.cpus)
        self.modify_attribute(self.name, "--memory", self.memory)
        for option in self.vmconfig.get_section():
            if option.startswith("bridgeadapter") or option.startswith("nic") or option.startswith("macaddress"):
                self.modify_attribute(self.name, "--" + option, self.vmconfig.get(option))
        
        self.modify_attribute(self.name, "--vrde", "on")


    def modify_attribute(self, uid, attribute, value):
        try:
            subprocess.check_call(["VBoxManage", "modifyvm", uid, attribute, value], stderr=open(os.devnull, 'w'))
        except subprocess.CalledProcessError as e:
            print "Could not set %s to %s for machine with uid %s." % (attribute, value, uid)
            pass

    def delete_vm(self, uid):
        if uid in (vm[1] for vm in self.manager.get_vms(True)):
            self.stop_vm(uid)
        try:
            proc = subprocess.check_call(["VBoxManage", "unregistervm", uid, "--delete"], stdout=open(os.devnull, 'w'), stderr=open(os.devnull, 'w'))
        except subprocess.CalledProcessError as e:
            print "Could not delete virtual machine with uid '%s'." % uid
            exit(1)
        
    def stop_vm(self, uid):
        proc = subprocess.check_call(["VBoxManage", "controlvm", uid, "poweroff"], stdout=open(os.devnull, 'w'), stderr=open(os.devnull, 'w'))
        while self.is_running(uid):
            sleep(0.1)

    def is_running(self, uid):
        for fields in (line.split() for line in subprocess.check_output(["ps", "ax"], stderr=open(os.devnull, 'w')).split("\n")):
            if len(fields) > 4 and ("VirtualBox" in fields[4] or "virtualbox" in fields[4]) and uid in fields:
                return True
        return False

def usage():
    usage = """
    -h --help                 Prints this help
    -f --config (configfile)  Configuration file
    -c --check                Check configuration
    """
    print usage

def main(argv):
    try:
        opt, args = getopt.getopt(argv, "hf:cl", ["help", "config=", "check", "listvms"])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    list_vms = False
    check_config = False
    uid = None
    create_vm = False
    configfile = "default.cfg"

    for option, value in opt:
        if option in ("-h", "--help"):
            usage()
            sys.exit()
        elif option in ("-f", "--config"):
            configfile = value
        elif option in ("-c", "--check"):
            check_config = True
        elif option in ("-l", "--list"):
            list_vms = True
        else:
            assert False, "unhandled option"
            
    check = SystemCheck()
    if not check.passed:
        exit(1)
    if not check.check_configfile(configfile):
        exit(1)
    if check_config:
        exit(0)

    manager = VMManager(check.machine_folder)
    if list_vms:
        manager.show_vms()

    vm = VM(manager, configfile)
        
        
if __name__ == "__main__":
    main(sys.argv[1:])
