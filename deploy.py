import subprocess
import os
import xml.sax.handler
from urllib import urlretrieve
class ConfigParser(xml.sax.handler.ContentHandler):
	def startElement(self, name, attributes):
		if name == "SystemProperties":
			self.defaultMachineFolder = attributes["defaultMachineFolder"]

class VM:
	def __init__(self, name, configfile):
		self.name = name
		self.folder = self.getDefaultMachineFolder(configfile) + "/" + self.name
		self.hdfile = self.folder + "/" + self.name + "-disk.vdi"

	def getDefaultMachineFolder(self, configfile):
		parser = xml.sax.make_parser()
		handler = ConfigParser()
		parser.setContentHandler(handler)
		parser.parse(configfile)
		return handler.defaultMachineFolder

	def stopVM(self):
		subprocess.call(["VBoxManage", "controlvm", self.name, "poweroff"])
		
	def unregisterVM(self, delete=True):
		self.stopVM()
		command = ["VBoxManage", "unregistervm", self.name]
		if (delete):
			command.append("--delete")
		subprocess.call(command)
		if (delete):
			subprocess.call(["rm", self.hdfile])

	def createVM(self):
		subprocess.call(["VBoxManage", "createvm", "--name", self.name, "--ostype", "Debian", "--register"])

	def modifyVM(self, attr, value):
		subprocess.call(["VBoxManage", "modifyvm", self.name, attr, value])
		
	def createHD(self, size, format):
		subprocess.call(["VBoxManage", "createhd", "--filename", self.hdfile, "--size", size, "--format", format])
		subprocess.call(["VBoxManage", "storagectl", self.name, "--name", "SATA Controller", "--add", "sata", "--controller", "IntelAhci"])
		subprocess.call(["VBoxManage", "storageattach", self.name, "--storagectl", "SATA Controller", "--port", "0", "--device", "0", "--type", "hdd", "--medium", self.hdfile])

	def setMemory(self, memory):
		self.modifyVM("--memory", memory)

	def configureNetwork(self, number, interface, type):
		self.modifyVM("--bridgeadapter" + number, interface)
		self.modifyVM("--nic" + number, type)

	def setVrde(self, status):
		self.modifyVM("--vrde", status)

class ISOLoader:
	def __init__(self, os=None, arch=None, version=None, url=None):
		self.os = os
		self.version = version
		self.url = url
		if self.os is None:
			self.os = "debian"
			self.arch = "amd64"
			self.version = "current"
		if self.os == "debian" and self.version == "current":
			self.version = self.getCurrentDebianVersion()

	def getCurrentDebianVersion(self):
		self.url = "http://cdimage.debian.org/debian-cd/current/" + self.arch + "/iso-cd/"


def main():
	vm = VM("adhocracy-vm")
	vm.unregisterVM()
	vm.createVM()
	vm.createHD("8000", "VDI")
	vm.setMemory("1024")
	vm.configureNetwork("1", "eth0", "bridged")
	vm.setVrde("on")
	

if __name__ == '__main__':
	main()
